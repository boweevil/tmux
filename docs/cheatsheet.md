# TMUX CHEATSHEET

## GENERAL

`prefix + ?` list key bindings

`prefix + :` run tmux command

`prefix + d` detach from session

`prefix + t` clock mode

## SESSIONS

`prefix + s` list/choose all sessions

`prefix + :new` create new session

`prefix + :new -s <mysession>` session named mysession

`prefix + $` rename session

## WINDOWS

`prefix + w` list/choose windows

`prefix + c` create new window

`prefix + <win id>` select window number

`prefix + l` select previously selected window

`prefix + n` next window

`prefix + p` previous window

`prefix + b` select last window

`prefix + &` kill window (but ask)

`prefix + ,` rename window

`prefix + .` mv window to #

`prefix + :movew -t 0:` move current window to session 0 keeping window name

`prefix + :movew -d 0:2` move window 2 from session 0 to current session

## PANES

`prefix + q` show id of panes

`prefix + "` horizontal split

`prefix + %` vertical split

`prefix + !` break pane out to a new window.

`prefix + <direction>` select pane in the direction of `<direction>`

`prefix + o` select next pane

`prefix + ^<direction>` resize pane in the direction of `<direction>`

`prefix + x` kill pane (but ask)

`prefix + space` next-layout

`prefix + {` mv pane left

`prefix + :join-pane -s 0:1` join pane 1 from session 0 window 1

`prefix + :join-pane -t 0:1` send current pane to session 0 window 1

`prefix + :break-pane` send current pane to a new window

## PLUGINS

### tmux-copycat

`prefix + /` search strings (regex works too)

`prefix + ctrl + f` simple file search

`prefix + ctrl + u` url search (http, ftp and git urls)

`prefix + alt + i` ip address search

`prefix + ctrl + d` number search (mnemonic d, as digit)

`prefix + alt + h` jumping over SHA-1 hashes (best used after git log command)

`prefix + ctrl + g` jumping over git status files (best used after git status command)

### tmux-logging

`prefix + P` toggle logging mode

`prefix + alt + p` take text screenshot

`prefix + alt + P` save the whole tmux history

`prefix + alt + c` clear the tmux history

### tmux-open

- copy mode

`o` open the selection in the appropiated program
    (determinated by open and xdg-open, personalizable)

`ctrl + o` open the selection with the default terminal $EDITOR

### tmux-sessionist

`prefix + g` prompt for session by name and switch to it

`prefix + C` prompt for creating new session by name

`prefix + X` kill session (but ask) without leaving tmux

`prefix + S` switch to last session

`prefix + @` promote current pane to a new session

### tmux-yank

`prefix + y` copies current line in the command line to the clipboard

- copy mode

`y` copy selection to the clipboard

`Y` "put" selection (copy & paste to cli and sync to clipboard)

`alt + y` "put" selection (copy & paste to cli)
