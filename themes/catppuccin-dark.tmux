#/* vim: set filetype=tmux : */
black="#181825"
gray="#313244"
black4="#585b70"

bg="#1e1e2e"
fg="#cdd6f4"
back="$bg"
blue="#89b4fa"
cyan="#89dceb"
green="#a6e3a1"
magenta="#cba6f7"
orange="#fab387"
red="#f38ba8"
violet="#f5c2e7"
yellow="#f9e2af"


source-file ~/.tmux/themes/catppuccin.tmux
