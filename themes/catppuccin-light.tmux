#/* vim: set filetype=tmux : */
black="#e6e9ef"
gray="#bcc0cc"
black4="#acb0be"

bg="#eff1f5"
fg="#4c4f69"
back="$bg"
blue="#1e66f5"
cyan="#179299"
green="#40a02b"
magenta="#ea76cb"
orange="#fe640b"
red="#d20f39"
violet="#8839ef"
yellow="#df8e1d"


source-file ~/.tmux/themes/catppuccin.tmux
