# ------------------------------------------------------------------------------
# Palette
# ------------------------------------------------------------------------------
background='#0f1318'
foreground='#e6e0ce'
selection_background='#25323f'
selection_foreground='#000000'

cursor='#bfbfbf'
cursor_text_color='#141414'

# Black
black='#0f1318'
brightblack='#676767'

# Red
red='#fe7633'
brightred='#ef7077'

# Green
green='#b8cb52'
brightgreen='#cbe545'

# Yellow
yellow='#feb353'
brightyellow='#ffed98'

# Blue
blue='#36a2d8'
brightblue='#6871ff'

# Magenta
magenta='#c930c7'
brightmagenta='#ff76ff'

# Cyan
cyan='#95e5ca'
brightcyan='#a6fce1'

# White
white='#c7c7c7'
brightwhite='#fffefe'

status_line_bg='#0B0E13'
status_line_fg='#B3B1AD'
status_level1_bg='#C6D863'
status_level1_fg='#3e424c'
status_level2_bg='#344255'
status_level2_fg='#C6D863'
status_prefix_level1_bg='#61B7E1'
status_prefix_level1_fg='#3e424c'
status_prefix_level2_bg='#344255'
status_prefix_level2_fg='#61B7E1'

# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------
#{{{
clock="$cyan"

statusLineBG="$status_line_bg"
statusLineFG="$status_line_fg"

borderNormalBG="$background"
borderNormalFG="$statusLineBG"
borderActiveBG="$background"
borderActiveFG="$cyan"

displayPaneNormal="$foreground"
displayPaneActive="$magenta"

messageModeBG="$statusLineBG"
messageModeFG="$magenta"

modeBG="$statusLineBG"
modeFG="$messageModeFG"

bellBG="$statusLineBG"
bellFG="$messageModeFG"

ActivityBG="$statusLineBG"
ActivityFG="$messageModeFG"

leftNormalBG0="$status_level1_bg"
leftNormalFG0="$status_level1_fg"
leftNormalBG1="$status_level2_bg"
leftNormalFG1="$status_level2_fg"

leftPrefixBG0="$status_prefix_level1_bg"
leftPrefixFG0="$status_prefix_level1_fg"
leftPrefixBG1="$status_prefix_level2_bg"
leftPrefixFG1="$status_prefix_level2_fg"

centerNormalBG="$statusLineBG"
centerNormalFG="$statusLineFG"
centerPrefixBG="$statusLineBG"
centerPrefixFG="$statusLineFG"
centerFlag="$brightwhite"

currentNormalBG="$statusLineBG"
currentNormalFG="$status_level1_bg"
currentPrefixBG="$statusLineBG"
currentPrefixFG="$status_prefix_level1_bg"
currentFlag="$red"

rightNormalBG0="$leftNormalBG0"
rightNormalFG0="$leftNormalFG0"
rightNormalBG1="$leftNormalBG1"
rightNormalFG1="$leftNormalFG1"

rightPrefixBG0="$leftPrefixBG0"
rightPrefixFG0="$leftPrefixFG0"
rightPrefixBG1="$leftPrefixBG1"
rightPrefixFG1="$leftPrefixFG1"
#}}}

source-file ~/.tmux/new_statusbar.tmux

# vim: set sw=4 ts=4 sts=4 et tw=80 ft=tmux fdm=marker fmr={{{,}}}:
