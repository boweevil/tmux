#/* vim: set filetype=tmux : */
# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------
clock="$blue"

statusLineBG="$black"
statusLineFG="$fg"

borderNormalBG="$back"
borderNormalFG="$statusLineBG"
borderActiveBG="$back"
borderActiveFG="$blue"

displayPaneNormal="$fg"
displayPaneActive="$blue"

messageModeBG="$statusLineBG"
messageModeFG="$violet"

modeBG="$statusLineBG"
modeFG="$messageModeFG"

bellBG="$statusLineBG"
bellFG="$messageModeFG"

ActivityBG="$statusLineBG"
ActivityFG="$messageModeFG"

leftNormalBG0="$blue"
leftNormalFG0="$back"
leftNormalBG1="$gray"
leftNormalFG1="$blue"

leftPrefixBG0="$green"
leftPrefixFG0="$back"
leftPrefixBG1="$gray"
leftPrefixFG1="$fg"

centerNormalBG="$statusLineBG"
centerNormalFG="$statusLineFG"
centerPrefixBG="$statusLineBG"
centerPrefixFG="$statusLineFG"
centerFlag="$black4"

currentNormalBG="$statusLineBG"
currentNormalFG="$blue"
currentPrefixBG="$statusLineBG"
currentPrefixFG="$green"
currentFlag="$magenta"

rightNormalBG0="$leftNormalBG0"
rightNormalFG0="$leftNormalFG0"
rightNormalBG1="$leftNormalBG1"
rightNormalFG1="$leftNormalFG1"

rightPrefixBG0="$leftPrefixBG0"
rightPrefixFG0="$leftPrefixFG0"
rightPrefixBG1="$leftPrefixBG1"
rightPrefixFG1="$leftPrefixFG1"

source-file ~/.tmux/new_statusbar.tmux
