#/* vim: set filetype=tmux : */
bg0="#2f383e"
bg1="#374247"
bg2="#404c51"
bg3="#4a555b"
bg4="#525c62"
bg5="#596763"
bg_visual="#573e4c"
bg_red="#544247"
bg_green="#445349"
bg_blue="#3b5360"
bg_yellow="#504f45"

fg="#d3c6aa"
red="#e67e80"
orange="#e69875"
yellow="#dbbc7f"
green="#a7c080"
aqua="#83c092"
blue="#7fbbb3"
purple="#d699b6"
grey0="#7a8478"
grey1="#859289"
grey2="#9da9a0"
statusline1="#a7c080"
statusline2="#d3c6aa"
statusline3="#e67e80"

clock="$green"

statusLineBG="$bg2"
statusLineFG="$grey1"

borderNormalBG="$bg0"
borderNormalFG="$statusLineBG"
borderActiveBG="$bg0"
borderActiveFG="$green"

displayPaneNormal="$bg4"
displayPaneActive="$green"

messageModeBG="$statusLineBG"
messageModeFG="$yellow"

modeBG="$statusLineBG"
modeFG="$yellow"

bellBG="$statusLineBG"
bellFG="$yellow"

ActivityBG="$statusLineBG"
ActivityFG="$yellow"

leftNormalBG0="$green"
leftNormalFG0="$bg0"
leftNormalBG1="$bg4"
leftNormalFG1="$grey2"

leftPrefixBG0="$red"
leftPrefixFG0="$bg0"
leftPrefixBG1="$grey1"
leftPrefixFG1="$bg1"

centerNormalBG="$statusLineBG"
centerNormalFG="$statusLineFG"
centerPrefixBG="$statusLineBG"
centerPrefixFG="$grey2"
centerFlag="$fg"

currentNormalBG="$statusLineBG"
currentNormalFG="$green"
currentPrefixBG="$statusLineBG"
currentPrefixFG="$red"
currentFlag="$yellow"

rightNormalBG0="$green"
rightNormalFG0="$bg0"
rightNormalBG1="$bg4"
rightNormalFG1="$grey2"

rightPrefixBG0="$red"
rightPrefixFG0="$bg0"
rightPrefixBG1="$grey1"
rightPrefixFG1="$bg1"

source-file ~/.tmux/new_statusbar.tmux
