# ------------------------------------------------------------------------------
# Theme
# ------------------------------------------------------------------------------
variant='dracula_pro'  #dracula_pro[_blade,_buffy,_lincoln,_morbius,_van_helsing]

# ------------------------------------------------------------------------------
# Palette
# ------------------------------------------------------------------------------
# Common colors  {{{
normal_red='#ff9580'
normal_green='#8aff80'
normal_yellow='#ffff80'
normal_blue='#9580ff'
normal_magenta='#ff80bf'
normal_cyan='#80ffea'
bright_red='#ffaa99'
bright_green='#a2ff99'
bright_yellow='#ffff99'
bright_blue='#aa99ff'
bright_magenta='#ff99cc'
bright_cyan='#99ffee'

fg='#f8f8f2'
bglighter='#393649'
bglight='#2e2b3b'
bg='#22212c'
bgdark='#17161d'
bgdarker='#0b0b0f'
comment='#7970a9'
selection='#454158'
subtle='#424450'
cyan='#80ffea'
green='#8aff80'
orange='#ffca80'
pink='#ff80bf'
purple='#9580ff'
red='#ff9580'
yellow='#ffff80'

#}}}
%if "#{==:$variant,dracula_pro}"  #{{{
  background='#22212c'
  foreground='#f8f8f2'
  cursor_text='#454158'
  cursor_cursor='#f8f8f2'
  selection_text='#f8f8f2'
  selection_background='#454158'
  normal_black='#22212c'
  normal_white='#f8f8f2'
  bright_black='#22212c'
  bright_white='#ffffff'
  #}}}
%elif "#{==:$variant,dracula_pro_blade}"  #{{{
  background='#212c2a'
  foreground='#f8f8f2'
  cursor_text='#415854'
  cursor='#f8f8f2'
  selection_text='#f8f8f2'
  selection_background='#415854'
  bglighter='#364946'
  bglight='#2B3B38'
  bg='#212C2A'
  bgdark='#161D1C'
  bgdarker='#0B0F0E'
  comment='#70A99F'
  selection='#415854'
  normal_black='#212c2a'
  normal_white='#f8f8f2'
  bright_black='#212c2a'
  bright_white='#ffffff'
  #}}}
%elif "#{==:$variant,dracula_pro_buffy}"  #{{{
  background='#2a212c'
  foreground='#f8f8f2'
  cursor_text='#544158'
  cursor='#f8f8f2'
  selection_text='#f8f8f2'
  selection_background='#544158'
  bglighter='#463649'
  bglight='#382B3B'
  bg='#2A212C'
  bgdark='#1C161D'
  bgdarker='#0E0B0F'
  comment='#9F70A9'
  selection='#544158'
  normal_black='#2a212c'
  normal_white='#f8f8f2'
  bright_black='#2a212c'
  bright_white='#ffffff'
  #}}}
%elif "#{==:$variant,dracula_pro_lincoln}"  #{{{
  background='#2c2a21'
  foreground='#f8f8f2'
  cursor_text='#585441'
  cursor='#f8f8f2'
  selection_text='#f8f8f2'
  selection_background='#585441'
  bglighter='#494636'
  bglight='#3B382B'
  bg='#2C2A21'
  bgdark='#1D1C16'
  bgdarker='#0F0E0B'
  comment='#A99F70'
  selection='#585441'
  normal_black='#2c2a21'
  normal_white='#f8f8f2'
  bright_black='#2c2a21'
  bright_white='#ffffff'
  #}}}
%elif "#{==:$variant,dracula_pro_morbius}"  #{{{
  background='#2c2122'
  foreground='#f8f8f2'
  cursor_text='#584145'
  cursor='#f8f8f2'
  selection_text='#f8f8f2'
  selection_background='#584145'
  bglighter='#493639'
  bglight='#3B2B2E'
  bg='#2C2122'
  bgdark='#1D1617'
  bgdarker='#0F0B0B'
  comment='#A97079'
  selection='#584145'
  normal_black='#2c2122'
  normal_white='#f8f8f2'
  bright_black='#2c2122'
  bright_white='#ffffff'
  #}}}
%elif "#{==:$variant,dracula_pro_van_helsing}"  #{{{
  background='#0b0d0f'
  foreground='#f8f8f2'
  cursor_text='#414d58'
  cursor='#f8f8f2'
  selection_text='#f8f8f2'
  selection_background='#414d58'
  bglighter='#161A1D'
  bglight='#111417'
  bg='#0B0D0F'
  bgdark='#070809'
  bgdarker='#000000'
  comment='#708CA9'
  selection='#414D58'
  normal_black='#0b0d0f'
  normal_white='#f8f8f2'
  bright_black='#0b0d0f'
  bright_white='#ffffff'
%endif  #}}}

# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------
#{{{
clock="$comment"

statusLineBG="$bglight"
statusLineFG="$comment"

borderNormalBG="$background"
borderNormalFG="$statusLineBG"
borderActiveBG="$background"
borderActiveFG="$comment"

displayPaneNormal="$comment"
displayPaneActive="$normal_magenta"

messageModeBG="$statusLineBG"
messageModeFG="$comment"

modeBG="$statusLineBG"
modeFG="$messageModeFG"

bellBG="$statusLineBG"
bellFG="$messageModeFG"

ActivityBG="$statusLineBG"
ActivityFG="$messageModeFG"

leftNormalBG0="$comment"
leftNormalFG0="$fg"
leftNormalBG1="$selection"
leftNormalFG1="$fg"

leftPrefixBG0="$green"
leftPrefixFG0="$background"
leftPrefixBG1="$selection"
leftPrefixFG1="$fg"

centerNormalBG="$statusLineBG"
centerNormalFG="$statusLineFG"
centerPrefixBG="$statusLineBG"
centerPrefixFG="$statusLineFG"
centerFlag="$bright_white"

currentNormalBG="$statusLineBG"
currentNormalFG="$foreground"
currentPrefixBG="$statusLineBG"
currentPrefixFG="$green"
currentFlag="$purple"

rightNormalBG0="$leftNormalBG0"
rightNormalFG0="$leftNormalFG0"
rightNormalBG1="$leftNormalBG1"
rightNormalFG1="$leftNormalFG1"

rightPrefixBG0="$leftPrefixBG0"
rightPrefixFG0="$leftPrefixFG0"
rightPrefixBG1="$leftPrefixBG1"
rightPrefixFG1="$leftPrefixFG1"
#}}}

source-file ~/.tmux/new_statusbar.tmux

# vim: set sw=4 ts=4 sts=4 et tw=80 ft=tmux fdm=marker fmr={{{,}}}:
