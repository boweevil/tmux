ui_base="colour18"
ui_alt="colour21"
accent_one="green"
accent_two="cyan"
accent_three="red"

# WINDOWS ---------------------------------------------------------------------
set-option -g clock-mode-colour               "$accent_one"

# PANES -----------------------------------------------------------------------
set-option -g pane-border-style               "fg=$ui_base"
set-option -g pane-active-border-style        "fg=$accent_one"
set-option -g display-panes-colour            "$ui_base"
set-option -g display-panes-active-colour     "$accent_one"

# STATUS BAR ------------------------------------------------------------------
set-option -g status-position                 bottom
set-option -g status-style                    "bg=$ui_base,fg=$accent_one"
set-option -g status-justify                  left

# Left
set-option -g status-left                     " #h:#S "
set-option -g status-left-style               "bg=$ui_base,fg=$ui_alt,bold"
set-option -g status-left-length              25

# Current
set-option -g window-status-format            " #I:#W:#{window_panes}#F#{?pane_synchronized,S,} "
set-option -g window-status-style             "bg=$ui_base,fg=$ui_alt"

set-option -g window-status-current-format    " #I:#W:#{window_panes}#F#{?pane_synchronized,S,} "
set-option -g window-status-current-style     "bg=$ui_base,fg=$accent_one,bold"

set-option -g window-status-activity-style    "bg=$ui_base,fg=$accent_one"
set-option -g window-status-bell-style        "bg=$ui_base,fg=$accent_one"
set-option -g window-status-separator         ""

# Right
set-option -g status-right                    "#{?client_prefix, #[fg=$accent_three]PREFIX , #(/bin/bash $HOME/.tmux/plugins/kube-tmux/kube.tmux 250 red cyan) }"
set-option -g status-right-style              "bg=$ui_base,fg=$accent_one,bold"
set-option -g status-right-length             50

# Other
set-option -g message-style                   "bg=$ui_base,fg=$accent_two"
set-option -g mode-style                      "bg=$ui_alt,fg=$ui_base"
