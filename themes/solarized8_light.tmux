base00='#839496'
base01='#93a1a1'
base02='#eee8d5'
base03='#fdf6e3'
base0='#657b83'
base1='#586e75'
base2='#073642'
base3='#002b36'
back="base03"
fg="$base03"
blue='#268bd2'
cyan='#2aa198'
green='#859900'
magenta='#d33682'
orange='#cb4b16'
red='#dc322f'
violet='#6c71c4'
yellow='#b58900'

source-file ~/.tmux/themes/solarized8.tmux
