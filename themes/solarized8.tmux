# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------
clock="$cyan"

statusLineBG="$base02"
statusLineFG="$base01"

borderNormalBG="$base03"
borderNormalFG="$statusLineBG"
borderActiveBG="$base03"
borderActiveFG="$cyan"

displayPaneNormal="$base01"
displayPaneActive="$cyan"

messageModeBG="$statusLineBG"
messageModeFG="$violet"

modeBG="$statusLineBG"
modeFG="$messageModeFG"

bellBG="$statusLineBG"
bellFG="$messageModeFG"

ActivityBG="$statusLineBG"
ActivityFG="$messageModeFG"

leftNormalBG0="$base1"
leftNormalFG0="$fg"
leftNormalBG1="$base00"
leftNormalFG1="$fg"

leftPrefixBG0="$red"
leftPrefixFG0="$fg"
leftPrefixBG1="$base00"
leftPrefixFG1="$fg"

centerNormalBG="$statusLineBG"
centerNormalFG="$statusLineFG"
centerPrefixBG="$statusLineBG"
centerPrefixFG="$base1"
centerFlag="$base3"

currentNormalBG="$statusLineBG"
currentNormalFG="$cyan"
currentPrefixBG="$statusLineBG"
currentPrefixFG="$red"
currentFlag="$orange"

rightNormalBG0="$leftNormalBG0"
rightNormalFG0="$leftNormalFG0"
rightNormalBG1="$leftNormalBG1"
rightNormalFG1="$leftNormalFG1"

rightPrefixBG0="$leftPrefixBG0"
rightPrefixFG0="$leftPrefixFG0"
rightPrefixBG1="$leftPrefixBG1"
rightPrefixFG1="$leftPrefixFG1"

source-file ~/.tmux/new_statusbar.tmux
