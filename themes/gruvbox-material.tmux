# ------------------------------------------------------------------------------
# Theme
# ------------------------------------------------------------------------------
gruvbox_material_theme='dark'  # [dark,light]
gruvbox_material_background='hard'  #[hard,medium,soft]
gruvbox_material_palette='material'  #[material,mix,original]

contrast='#{gruvbox_material_theme}_#{gruvbox_material_background}'
palette='#{gruvbox_material_theme}_#{gruvbox_material_palette}'

# ------------------------------------------------------------------------------
# Palette
# ------------------------------------------------------------------------------
%if "#{==:$contrast,dark_hard}"  #{{{
    bg0='#1d2021'
    bg1='#282828'
    bg2='#282828'
    bg3='#3c3836'
    bg4='#3c3836'
    bg5='#504945'
    bg_statusline1='#282828'
    bg_statusline2='#32302f'
    bg_statusline3='#504945'
    bg_diff_green='#32361a'
    bg_visual_green='#333e34'
    bg_diff_red='#3c1f1e'
    bg_visual_red='#442e2d'
    bg_diff_blue='#0d3138'
    bg_visual_blue='#2e3b3b'
    bg_visual_yellow='#473c29'
    bg_current_word='#32302f'  #}}}
%elif "#{==:$contrast,light_hard}"  #{{{
    bg0='#f9f5d7'
    bg1='#f5edca'
    bg2='#f3eac7'
    bg3='#f2e5bc'
    bg4='#eee0b7'
    bg5='#ebdbb2'
    bg_statusline1='#f5edca'
    bg_statusline2='#f3eac7'
    bg_statusline3='#eee0b7'
    bg_diff_green='#e4edc8'
    bg_visual_green='#dde5c2'
    bg_diff_red='#f8e4c9'
    bg_visual_red='#f0ddc3'
    bg_diff_blue='#e0e9d3'
    bg_visual_blue='#d9e1cc'
    bg_visual_yellow='#f9eabf'
    bg_current_word='#f3eac7'  #}}}
%elif "#{==:$contrast,dark_medium}"  #{{{
    bg0='#282828'
    bg1='#32302f'
    bg2='#32302f'
    bg3='#45403d'
    bg4='#45403d'
    bg5='#5a524c'
    bg_statusline1='#32302f'
    bg_statusline2='#3a3735'
    bg_statusline3='#504945'
    bg_diff_green='#34381b'
    bg_visual_green='#3b4439'
    bg_diff_red='#402120'
    bg_visual_red='#4c3432'
    bg_diff_blue='#0e363e'
    bg_visual_blue='#374141'
    bg_visual_yellow='#4f422e'
    bg_current_word='#3c3836'  #}}}
%elif "#{==:$contrast,light_medium}"  #{{{
    bg0='#fbf1c7'
    bg1='#f4e8be'
    bg2='#f2e5bc'
    bg3='#eee0b7'
    bg4='#e5d5ad'
    bg5='#ddccab'
    bg_statusline1='#f2e5bc'
    bg_statusline2='#f2e5bc'
    bg_statusline3='#e5d5ad'
    bg_diff_green='#e6eabc'
    bg_visual_green='#dee2b6'
    bg_diff_red='#f9e0bb'
    bg_visual_red='#f1d9b5'
    bg_diff_blue='#e2e6c7'
    bg_visual_blue='#dadec0'
    bg_visual_yellow='#fae7b3'
    bg_current_word='#f2e5bc'  #}}}
%elif "#{==:$contrast,dark_soft}"  #{{{
    bg0='#32302f'
    bg1='#3c3836'
    bg2='#3c3836'
    bg3='#504945'
    bg4='#504945'
    bg5='#665c54'
    bg_statusline1='#3c3836'
    bg_statusline2='#46413e'
    bg_statusline3='#5b534d'
    bg_diff_green='#3d4220'
    bg_visual_green='#424a3e'
    bg_diff_red='#472322'
    bg_visual_red='#543937'
    bg_diff_blue='#0f3a42'
    bg_visual_blue='#404946'
    bg_visual_yellow='#574833'
    bg_current_word='#45403d'  #}}}
%elif "#{==:$contrast,light_soft}"  #{{{
    bg0='#f2e5bc'
    bg1='#eddeb5'
    bg2='#ebdbb2'
    bg3='#e6d5ae'
    bg4='#dac9a5'
    bg5='#d5c4a1'
    bg_statusline1='#ebdbb2'
    bg_statusline2='#ebdbb2'
    bg_statusline3='#dac9a5'
    bg_diff_green='#dfe1b4'
    bg_visual_green='#d7d9ae'
    bg_diff_red='#f7d9b9'
    bg_visual_red='#efd2b3'
    bg_diff_blue='#dbddbf'
    bg_visual_blue='#d3d5b8'
    bg_visual_yellow='#f3deaa'
    bg_current_word='#ebdbb2'
%endif  #}}}

%if "#{==:$palette,dark_material}"  #{{{
    fg0='#d4be98'
    fg1='#ddc7a1'
    red='#ea6962'
    orange='#e78a4e'
    yellow='#d8a657'
    green='#a9b665'
    aqua='#89b482'
    blue='#7daea3'
    purple='#d3869b'
    bg_red='#ea6962'
    bg_green='#a9b665'
    bg_yellow='#d8a657'  #}}}
%elif "#{==:$palette,light_material}"  #{{{
    fg0='#654735'
    fg1='#4f3829'
    red='#c14a4a'
    orange='#c35e0a'
    yellow='#b47109'
    green='#6c782e'
    aqua='#4c7a5d'
    blue='#45707a'
    purple='#945e80'
    bg_red='#ae5858'
    bg_green='#6f8352'
    bg_yellow='#a96b2c'  #}}}
%elif "#{==:$palette,dark_mix}"  #{{{
    fg0='#e2cca9'
    fg1='#e2cca9'
    red='#f2594b'
    orange='#f28534'
    yellow='#e9b143'
    green='#b0b846'
    aqua='#8bba7f'
    blue='#80aa9e'
    purple='#d3869b'
    bg_red='#db4740'
    bg_green='#b0b846'
    bg_yellow='#e9b143'  #}}}
%elif "#{==:$palette,light_mix}"  #{{{
    fg0='#514036'
    fg1='#514036'
    red='#af2528'
    orange='#b94c07'
    yellow='#b4730e'
    green='#72761e'
    aqua='#477a5b'
    blue='#266b79'
    purple='#924f79'
    bg_red='#ae5858'
    bg_green='#6f8352'
    bg_yellow='#a96b2c'  #}}}
%elif "#{==:$palette,dark_original}"  #{{{
    fg0='#ebdbb2'
    fg1='#ebdbb2'
    red='#fb4934'
    orange='#fe8019'
    yellow='#fabd2f'
    green='#b8bb26'
    aqua='#8ec07c'
    blue='#83a598'
    purple='#d3869b'
    bg_red='#cc241d'
    bg_green='#b8bb26'
    bg_yellow='#fabd2f'  #}}}
%elif "#{==:$palette,light_original}"  #{{{
    fg0='#3c3836'
    fg1='#3c3836'
    red='#9d0006'
    orange='#af3a03'
    yellow='#b57614'
    green='#79740e'
    aqua='#427b58'
    blue='#076678'
    purple='#8f3f71'
    bg_red='#ae5858'
    bg_green='#6f8352'
    bg_yellow='#a96b2c'
%endif  #}}}

%if "#{==:$gruvbox_material_theme,dark}" #{{{
    grey0='#7c6f64'
    grey1='#928374'
    grey2='#a89984'
%else
    grey0='#a89984'
    grey1='#928374'
    grey2='#7c6f64'
%endif #}}}

# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------
#{{{
clock="$aqua"

statusLineBG="$bg_statusline1"
statusLineFG="$grey1"

borderNormalBG="$bg0"
borderNormalFG="$statusLineBG"
borderActiveBG="$bg0"
borderActiveFG="$aqua"

displayPaneNormal="$bg4"
displayPaneActive="$aqua"

messageModeBG="$statusLineBG"
messageModeFG="$purple"

modeBG="$statusLineBG"
modeFG="$messageModeFG"

bellBG="$statusLineBG"
bellFG="$messageModeFG"

ActivityBG="$statusLineBG"
ActivityFG="$messageModeFG"

leftNormalBG0="$grey2"
leftNormalFG0="$bg0"
leftNormalBG1="$bg_statusline3"
leftNormalFG1="$fg1"

leftPrefixBG0="$red"
leftPrefixFG0="$bg0"
leftPrefixBG1="$grey2"
leftPrefixFG1="$bg0"

centerNormalBG="$statusLineBG"
centerNormalFG="$statusLineFG"
centerPrefixBG="$statusLineBG"
centerPrefixFG="$grey1"
centerFlag="$fg1"

currentNormalBG="$statusLineBG"
currentNormalFG="$aqua"
currentPrefixBG="$statusLineBG"
currentPrefixFG="$red"
currentFlag="$orange"

rightNormalBG0="$leftNormalBG0"
rightNormalFG0="$leftNormalFG0"
rightNormalBG1="$leftNormalBG1"
rightNormalFG1="$leftNormalFG1"

rightPrefixBG0="$leftPrefixBG0"
rightPrefixFG0="$leftPrefixFG0"
rightPrefixBG1="$leftPrefixBG1"
rightPrefixFG1="$leftPrefixFG1"
#}}}

source-file ~/.tmux/new_statusbar.tmux

# vim: set sw=4 ts=4 sts=4 et tw=80 ft=tmux fdm=marker fmr={{{,}}}:
