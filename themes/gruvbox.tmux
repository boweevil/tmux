#/* vim: set filetype=tmux : */
dark0_hard='#1d2021'
dark0='#282828'
dark0_soft='#32302f'
dark1='#3c3836'
dark2='#504945'
dark3='#665c54'
dark4='#7c6f64'
dark4_256='#7c6f64'

gray_245='#928374'
gray_244='#928374'

light0_hard='#f9f5d7'
light0='#fbf1c7'
light0_soft='#f2e5bc'
light1='#ebdbb2'
light2='#d5c4a1'
light3='#bdae93'
light4='#a89984'
light4_256='#a89984'

bright_red='#fb4934'
bright_green='#b8bb26'
bright_yellow='#fabd2f'
bright_blue='#83a598'
bright_purple='#d3869b'
bright_aqua='#8ec07c'
bright_orange='#fe8019'

neutral_red='#cc241d'
neutral_green='#98971a'
neutral_yellow='#d79921'
neutral_blue='#458588'
neutral_purple='#b16286'
neutral_aqua='#689d6a'
neutral_orange='#d65d0e'

faded_red='#9d0006'
faded_green='#79740e'
faded_yellow='#b57614'
faded_blue='#076678'
faded_purple='#8f3f71'
faded_aqua='#427b58'
faded_orange='#af3a03'


clock="$neutral_aqua"

statusLineBG="$dark1"
statusLineFG="$dark4"

borderNormalBG="$dark0"
borderNormalFG="$statusLineBG"
borderActiveBG="$dark0"
borderActiveFG="$neutral_aqua"

displayPaneNormal="$dark4"
displayPaneActive="$neutral_aqua"

messageModeBG="$statusLineBG"
messageModeFG="$bright_purple"

modeBG="$statusLineBG"
modeFG="$messageModeFG"

bellBG="$statusLineBG"
bellFG="$messageModeFG"

ActivityBG="$statusLineBG"
ActivityFG="$messageModeFG"

leftNormalBG0="$light4"
leftNormalFG0="$dark1"
leftNormalBG1="$dark2"
leftNormalFG1="$light4"

leftPrefixBG0="$bright_red"
leftPrefixFG0="$dark0"
leftPrefixBG1="$light4"
leftPrefixFG1="$dark0"

centerNormalBG="$statusLineBG"
centerNormalFG="$statusLineFG"
centerPrefixBG="$statusLineBG"
centerPrefixFG="$light4"
centerFlag="$light1"

currentNormalBG="$statusLineBG"
currentNormalFG="$bright_aqua"
currentPrefixBG="$statusLineBG"
currentPrefixFG="$bright_red"
currentFlag="$bright_orange"

rightNormalBG0="$leftNormalBG0"
rightNormalFG0="$leftNormalFG0"
rightNormalBG1="$leftNormalBG1"
rightNormalFG1="$leftNormalFG1"

rightPrefixBG0="$leftPrefixBG0"
rightPrefixFG0="$leftPrefixFG0"
rightPrefixBG1="$leftPrefixBG1"
rightPrefixFG1="$leftPrefixFG1"

source-file ~/.tmux/new_statusbar.tmux
