#!/usr/bin/env python3

import os
from pathlib import Path
import subprocess

#import git

# Custom tasks for TMUX -------------------------------------------------------
print("Setting up TMUX...")

HOME = str(Path.home())
TMUX_PLUGINS_DIR = f"{HOME}/.tmux/plugins"
TMUX_PLUGINS = {
    f"{TMUX_PLUGINS_DIR}/tmux-copycat": "https://github.com/tmux-plugins/tmux-copycat.git",
    f"{TMUX_PLUGINS_DIR}/tmux-logging": "https://github.com/tmux-plugins/tmux-logging.git",
    f"{TMUX_PLUGINS_DIR}/tmux-open": "https://github.com/tmux-plugins/tmux-open.git",
    f"{TMUX_PLUGINS_DIR}/tmux-sessionist": "https://github.com/tmux-plugins/tmux-sessionist.git",
    f"{TMUX_PLUGINS_DIR}/tmux-yank": "https://github.com/tmux-plugins/tmux-yank.git",
}

if not os.path.isdir(TMUX_PLUGINS_DIR):
    os.makedirs(TMUX_PLUGINS_DIR, exist_ok=True)

for key, plugin in TMUX_PLUGINS.items():
    if not os.path.isdir(key):
        try:
            print(f"Installing {plugin}...")
            subprocess.run(
                f"git clone {plugin} {key}",
                shell=True,
                check=True,
                cwd=TMUX_PLUGINS_DIR
            )
        except BaseException:
            print(f"Something failed with the installation of {plugin}.")
