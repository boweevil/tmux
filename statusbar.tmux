#/* vim: set filetype=tmux : */
# WINDOWS ---------------------------------------------------------------------
setw -g clock-mode-colour               "$AN2"
setw -g window-status-bell-style        "bg=$BG0,fg=$AN2"

# PANES -----------------------------------------------------------------------
set -g pane-border-style                "fg=$BG1"
set -g pane-active-border-style         "fg=$AN2"
set -g display-panes-colour             "$FG0"
set -g display-panes-active-colour      "$AN2"

# STATUS BAR ------------------------------------------------------------------
set -g status-position bottom
set -g status-style                     "bg=$BG1,fg=$FG0"
set -g status-justify left

# Left
setw -g status-left                     "#{?client_prefix,\
#[bg=$AP1]#[fg=$FG0] #h #[bg=$CN1]#[fg=$AP1]\
#[bg=$CN1]#[fg=$BG0] #S #[bg=$BG1]#[fg=$CN1]\
,\
#[bg=$CN1]#[fg=$BG0] #h #[bg=$CN2]#[fg=$CN1]\
#[bg=$CN2]#[fg=$CN1] #S #[bg=$BG1]#[fg=$CN2]\
}"

setw -g status-left-style               "bg=$FG0,fg=$BG0,bold"
setw -g status-left-length 50

# Current
setw -g window-status-format            "#{?client_prefix,\
#[bg=$CN1]#[fg=$BG1]#[bg=$CN1]#[fg=$BG0] #I:#W:#{window_panes}#F#{?pane_synchronized,#[fg=$AP1]S,} #[bg=$BG1]#[fg=$CN1]\
,\
#[bg=$CN2]#[fg=$BG1]#[bg=$CN2]#[fg=$CN1] #I:#W:#{window_panes}#F#{?pane_synchronized,#[fg=$AN2]S,} #[bg=$BG1]#[fg=$CN2]\
}"

setw -g window-status-current-format    "#{?client_prefix,\
#[bg=$AP2]#[fg=$BG1]#[fg=$BG0] #I:#W:#{window_panes}#F#{?pane_synchronized,S,} #[bg=$BG1]#[fg=$AP2]\
,\
#[bg=$AN1]#[fg=$BG1]#[fg=$BG0] #I:#W:#{window_panes}#F#{?pane_synchronized,S,} #[bg=$BG1]#[fg=$AN1]\
}"

setw -g window-status-current-style     "bg=$BG0,fg=$AN1,bold"
setw -g window-status-activity-style    "bg=$AN2,fg=$BG0"
setw -g window-status-bell-style        "bg=$AN2,fg=$BG0"
setw -g window-status-separator         ""

# Right
setw -g status-right                    "#{?client_prefix,\
#[bg=$BG1]#[fg=$CN1]#[bg=$CN1]#[fg=$BG0] #($HOME/.tmux/scripts/kube.sh ns) \
#[bg=$CN1]#[fg=$AP1]#[bg=$AP1]#[fg=$FG0] #($HOME/.tmux/scripts/kube.sh ctx) \
,\
#[bg=$BG1]#[fg=$CN2]#[bg=$CN2]#[fg=$CN1] #($HOME/.tmux/scripts/kube.sh ns) \
#[bg=$CN2]#[fg=$CN1]#[bg=$CN1]#[fg=$BG0] #($HOME/.tmux/scripts/kube.sh ctx) \
}"

setw -g status-right-style              "bg=$FG0,fg=$BG0,bold"
setw -g status-right-length 50

# Other
set -g message-style                    "bg=$AN2,fg=$BG0"
set -g mode-style                       "bg=$AN2,fg=$BG0"
