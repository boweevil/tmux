#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

_usage() {
	echo "USAGE: kube.sh <context|namespace>"
	echo " ctx, context     Return the current context."
	echo " ns, namespace    Return the current namespace."
}


_check_kubectl() {
	if ! command -v kubectl &>/dev/null; then
		echo "Kubectl not found..."
		return 1
	fi
}


_get_context() {
    kubectl config current-context \
		| awk '{ print " " $1 " " }' \
		|| echo ""
}


_get_namespace() {
	kubectl describe sa default \
		| awk '/^Namespace/ {print " " $2 " "}' \
		|| echo ""
}


main() {
	_check_kubectl || exit 1
	local namespace
	namespace="$(_get_namespace)"
	case ${1:-} in
		'ns'|'namespace')
			echo "${namespace}" || exit 1
		;;
		'ctx'|'context' )
			if [[ -z "${namespace}" ]]; then
				echo "" && exit 1
			fi
			_get_context || exit 1
		;;
		* )
			_usage
	esac
}

main "$@"
