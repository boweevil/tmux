#/* vim: set filetype=tmux : */
# WINDOWS ---------------------------------------------------------------------
setw -g clock-mode-colour               "$clock"
setw -g window-status-bell-style        "bg=$bellBG,fg=$bellFG"

# PANES -----------------------------------------------------------------------
set -g pane-border-style                "bg=$borderNormalBG,fg=$borderNormalFG"
set -g pane-active-border-style         "bg=$borderActiveBG,fg=$borderActiveFG"
set -g display-panes-colour             "$displayPaneNormal"
set -g display-panes-active-colour      "$displayPaneActive"

# STATUS BAR ------------------------------------------------------------------
set -g status-position bottom
set -g status-style                     "bg=$statusLineBG,fg=$statusLineFG"
set -g status-justify left

# Left
setw -g status-left                     "#{?client_prefix,\
#[bg=$leftPrefixBG0]#[fg=$leftPrefixFG0] #h #[bg=$leftPrefixBG1]#[fg=$leftPrefixBG0]\
#[bg=$leftPrefixBG1]#[fg=$leftPrefixFG1] #S #[bg=$statusLineBG]#[fg=$leftPrefixBG1]\
,\
#[bg=$leftNormalBG0]#[fg=$leftNormalFG0] #h #[bg=$leftNormalBG1]#[fg=$leftNormalBG0]\
#[bg=$leftNormalBG1]#[fg=$leftNormalFG1] #S #[bg=$statusLineBG]#[fg=$leftNormalBG1]\
}"

setw -g status-left-style               "bg=$statusLineBG,fg=$statusLineFG"
setw -g status-left-length 50

# Current
setw -g window-status-format            "#{?client_prefix,\
#[fg=$centerPrefixFG] #I:#W.#(basename #{pane_current_path}):#{window_panes}#[fg=$centerFlag]#F#{?pane_synchronized,S,} \
,\
 #I:#W.#(basename #{pane_current_path}):#{window_panes}#[fg=$centerFlag]#F#{?pane_synchronized,S,} \
}"

setw -g window-status-current-format    "#{?client_prefix,\
#[fg=$currentPrefixFG] #I:#W.#(basename #{pane_current_path}):#{window_panes}#[fg=$currentFlag]#F#{?pane_synchronized,S,} \
,\
 #I:#W.#(basename #{pane_current_path}):#{window_panes}#[fg=$currentFlag]#F#{?pane_synchronized,S,} \
}"

setw -g window-status-current-style     "bg=$currentNormalBG,fg=$currentNormalFG,bold"
setw -g window-status-activity-style    "bg=$ActivityBG,fg=$ActivityFG"
setw -g window-status-bell-style        "bg=$bellBG,fg=$bellFG"
setw -g window-status-separator         ""

# Right
setw -g status-right                    "#{?client_prefix,\
#[bg=$statusLineBG]#[fg=$rightPrefixBG1]#[bg=$rightPrefixBG1]#[fg=$rightPrefixFG1]#($HOME/.tmux/scripts/kube.sh ns)\
#[bg=$rightPrefixBG1]#[fg=$rightPrefixBG0]#[bg=$rightPrefixBG0]#[fg=$rightPrefixFG0]#($HOME/.tmux/scripts/kube.sh ctx)\
,\
#[bg=$statusLineBG]#[fg=$rightNormalBG1]#[bg=$rightNormalBG1]#[fg=$rightNormalFG1]#($HOME/.tmux/scripts/kube.sh ns)\
#[bg=$rightNormalBG1]#[fg=$rightNormalBG0]#[bg=$rightNormalBG0]#[fg=$rightNormalFG0]#($HOME/.tmux/scripts/kube.sh ctx)\
}"

setw -g status-right-style              "bg=$statusLineBG,fg=$statusLineFG"
setw -g status-right-length 50

# Other
set -g message-style                    "bg=$messageModeBG,fg=$messageModeFG"
set -g mode-style                       "bg=$modeBG,fg=$modeFG"
